# Support

> Support for [solidweb.me](https://solidweb.me/) Community Solid Server

This repository is a work in progress.  

Currently, the aims are:

- **[Knowledge base (Github repo)](https://github.com/CommunitySolidServer/CommunitySolidServer):** Information about the project
- **[New issues](https://gitlab.com/solidweb.me/support/-/issues/new):** Add a support issue
- **[History](https://gitlab.com/solidweb.me/support/issues):** Track what was done and when

[**File an issue →**](https://gitlab.com/solidweb.me/support/-/issues/new)

Issues raised here should be of a short lived support nature.  Maybe a 'proposals' category for longer lived issues would make sense.
